---
description: "Places to chat with fellow Ballafornians!"
---

Below are some links where you can chat with fellow residents of Ballarat. If
you know of more or have links of your own to add please submit them via
[gitlab](https://gitlab.com/ballarat-hackerspace/websites/ballarat.chat/-/issues)
or [email](mailto:committee@ballarathackerspace.org.au) them to us.

This site is provided by the [Ballarat
Hackerspace](https://ballarathackerspace.org.au), if you have any interest in
electronics, robotics, 3d printing, programming or many other related topics be
sure to drop in and say hello! :heart:

You may also be interested in [ballarat.social](https://ballarat.social)!
