# ballarat.chat

## Local Development

Ensure you have `hugo` and `go` installed, checkout the repo and then run the following:

```
$ cd ballarat.chat
$ hugo mod init gitlab.com/ballarat-hackerspace/websites/ballarat.chat
$ hugo server
```

Now browse to [localhost:1313](http://localhost:1313)

Make any changes, e.g. adding/removing/updating links in the `config.toml` page.

## Deployment

Is handled by the `.gitlab-ci.yml` file and the site is served by GitLab pages.
